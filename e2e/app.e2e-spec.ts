import { TinyTwitterPage } from './app.po';

describe('tiny-twitter App', () => {
  let page: TinyTwitterPage;

  beforeEach(() => {
    page = new TinyTwitterPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
