export class User {
	id: any;
	email: String;
	username: String;
	givenName: String;
	familyName: String;
	inscriptionDate: Date;
	nbFollowers: number;

	constructor() {
		
	}
	getFullName() {
		return this.familyName + ' ' + this.givenName;
	}

	
}
