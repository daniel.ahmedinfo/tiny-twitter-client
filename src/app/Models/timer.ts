export class Timer {
	private _secondes: number = 0;
	private _millisecondes: number = 0;
	private _totalSecondes: number = 0;
	private _timer;

	get secondes(): number {
		return this._secondes;
	}
	get millisecondes(): number {
		return this._millisecondes;
	}
	start() {
		this._timer = setInterval(() => {
			//this._minutes = Math.floor(++this._totalSecondes / 60);
			//this._secondes = this._totalSecondes - this._minutes * 60;
			this._secondes = Math.floor(++this._totalSecondes / 1000);
			this._millisecondes = ++this._totalSecondes - this._secondes;
		}, 1);
	}
	stop() {
		clearInterval(this._timer);
	}
	reset() {
		this._totalSecondes = this._secondes = this._millisecondes = 0;
	}
}
