import { User } from './user';
export class Post {
	sender: User;
	creationDate: Date;
	body: {
		value: String;
	};

	constructor() {
		this.creationDate = null;
		this.sender = null;
		this.body = {
			value: null
		};
	}
}
