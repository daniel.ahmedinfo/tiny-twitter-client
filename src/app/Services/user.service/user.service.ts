import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { catchError, map, tap } from 'rxjs/operators';
import { User } from 'app/Models/user';
import { environment } from 'environments/environment.prod';

declare const gapi: any;

@Injectable()
export class UserService {
	private BASE = environment.apiUrl + environment.personEndpoint;

	constructor(private http: Http) {
		//this.gapiGetAllUser();
	}

	public gapiGetAllUser(top: boolean = false, limit: number = 10) {
		gapi.load('client:auth2', () => {
			var rootApi = 'http://cloudproject-186814.appspot.com/_ah/api/';
			gapi.client.load(
				'person',
				'v1',
				() => {
					console.log('score api loaded');
					gapi.client.person.allUsers().execute(resp => {
						console.log(resp);
					});
				},
				rootApi
			);
		});
	}

	public getAllUsers(top: boolean = false, limit: number = 10) {
		let url = top ? this.BASE + '?toptweeter=true&' : '';
		url = url + '?limit=' + limit;
		return this.http.get(url);
	}

	public getProfile(username: String) {
		let url = this.BASE + '/' + username;
		return this.http.get(url);
	}

	public addUser(user: User) {
		let url = this.BASE;
		return this.http.post(this.BASE, user);
	}

	public follow(myUsername: String, usernameFolow) {
		let url = this.BASE + '/' + myUsername + '/followers/' + usernameFolow;
		return this.http.put(url, {});
	}

	public getfollowing(myUsername: String) {
		let url = this.BASE + '/' + myUsername + '/following';
		return this.http.get(url);
	}

	public getTimeline(myUsername: String, limit: number = 10) {
		let url = this.BASE + '/' + myUsername + '/posts?limit=' + limit;

		return this.http.get(url);
	}
}
