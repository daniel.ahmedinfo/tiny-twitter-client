import { Injectable } from '@angular/core';
import { CanActivate } from '@angular/router/src/interfaces';
import { DataService } from '../data.service/data.service';
import { Router } from '@angular/router';

@Injectable()
export class AuthGuardService implements CanActivate {
	constructor(private dataService: DataService, private router: Router) {}

	canActivate() {
		if (localStorage.getItem('currentUser')) {
			return true;
		}
		this.router.navigate(['']);
		return false;
	}
}
