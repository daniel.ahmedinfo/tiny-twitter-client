import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Post } from '../../Models/post';
import { environment } from '../../../environments/environment.prod';

@Injectable()
export class PostService {
	private BASE = environment.apiUrl + environment.postEndpoint;

	constructor(private http: Http) {}

	public getAllMessages(limit: number = 100) {
		let url = this.BASE + '/posts?limit=' + limit;
		return this.http.get(url);
	}

	public tweet(user: String, post: Post) {
		let url = this.BASE + '/users/' + user + '/posts';
		return this.http.post(url, post);
	}
}
