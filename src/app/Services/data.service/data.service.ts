import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { User } from 'app/Models/user';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class DataService {
	following: Array<User>;
	currentUser: User;

	constructor() {
		this.following = new Array<User>();
		this.currentUser = new User();
	}

	changeFollowing(following: Array<User>) {
		this.following = following;
	}

	changeUser(user: User) {
		this.currentUser = user;
	}
}
