import { Component, OnInit } from '@angular/core';
import { Post } from '../../Models/post';
import { PostService } from 'app/Services/post.service/post.service';
import { Timer } from 'app/Models/timer';
import { DataService } from 'app/Services/data.service/data.service';
import { DialogComponent, DialogService } from 'ng2-bootstrap-modal';

export interface ConfirmModel {
	title: string;
	message: string;
}

@Component({
	selector: 'app-tweet',
	templateUrl: './tweet.component.html',
	styleUrls: ['./tweet.component.css']
})
export class TweetComponent extends DialogComponent<ConfirmModel, boolean> implements ConfirmModel, OnInit {
	post: Post = new Post();
	chrono: number;
	title: string;
	message: string;

	constructor(private postService: PostService, private dataService: DataService, dialogService: DialogService) {
		super(dialogService);
	}

	ngOnInit() {}

	tweet() {
		let dateDeb = new Date();
		this.postService.tweet(JSON.parse(localStorage.getItem('currentUser')).username, this.post).subscribe(
			complete => {
				let dateFin = new Date();
				this.chrono = dateFin.getTime() - dateDeb.getTime();
				this.post.body.value = '';
				console.log(complete.json());
				setTimeout(() => {
					this.confirm();
				}, 30000);
			},
			err => {
				console.log(err);
			}
		);
	}

	confirm() {
		// we set dialog result as true on click on confirm button,
		// then we can get dialog result from caller code
		this.result = true;
		this.close();
	}
}
