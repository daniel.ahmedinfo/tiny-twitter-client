import { Component, OnInit } from '@angular/core';
import { UserService } from '../../Services/user.service/user.service';
import { Timer } from '../../Models/timer';
import { Input } from '@angular/core';
import { User } from 'app/Models/user';
import { Output } from '@angular/core';
import { EventEmitter } from '@angular/core';
import { ViewEncapsulation } from '@angular/core';
import { ChangeDetectionStrategy } from '@angular/core';
import { DataService } from '../../Services/data.service/data.service';
import { setInterval } from 'timers';

@Component({
	selector: 'app-follow',
	templateUrl: './follow.component.html',
	styleUrls: ['./follow.component.css']
})
export class FollowComponent implements OnInit {
	@Input() user1: String;
	@Input() user2: String;
	@Input() following: Array<User>;

	chrono: number;
	allreadyFollowed: boolean;
	constructor(private userService: UserService, private dataService: DataService) {
		this.chrono = 0;
		this.allreadyFollowed = false;
		console.log(this.user2);
	}

	ngOnInit() {
		//this.isFollowed();
		setInterval(() => {
			if (this.following) {
				this.following = JSON.parse(localStorage.getItem('following'));
				this.isFollowed();
			}
		}, 100);
	}

	public isFollowed() {
		if (this.following) {
			let size: number = this.following.length;
			let i: number = 0;
			while (!this.allreadyFollowed && i < size) {
				if (this.following[i].username == this.user2) {
					this.allreadyFollowed = true;
				}
				i++;
			}
		}
	}

	public followSomeOne() {
		let dateDeb = new Date();
		this.userService.follow(this.user1, this.user2).subscribe(
			complete => {
				let dateFin = new Date();
				this.chrono = dateFin.getTime() - dateDeb.getTime();
				this.following = complete.json().items;
				this.isFollowed();
				localStorage.setItem('following', JSON.stringify(this.following));
				if (this.following && this.following.length == 1) {
					setInterval(() => {
						if (this.following) {
							this.following = JSON.parse(localStorage.getItem('following'));
							this.isFollowed();
						}
					}, 100);
				}
			},
			err => {
				this.isFollowed();
				console.log(err);
			}
		);
	}
}
