import { Component, OnInit } from '@angular/core';
import { UserService } from '../../Services/user.service/user.service';
import { PostService } from 'app/Services/post.service/post.service';
import { Post } from 'app/Models/post';
import { Timer } from '../../Models/timer';
import { User } from '../../Models/user';
import { EventEmitter } from 'events';
import { Router } from '@angular/router';
import { ChangeDetectorRef } from '@angular/core';
import { DataService } from '../../Services/data.service/data.service';

@Component({
	selector: 'app-tweets',
	templateUrl: './tweets.component.html',
	styleUrls: ['./tweets.component.css']
})
export class TweetsComponent implements OnInit {
	tweets: Array<Post>;
	limit: number = 100;
	chrono: Timer = new Timer();
	following: Array<User>;
	user: User;

	constructor(
		private userService: UserService,
		private postService: PostService,
		private route: Router,
		private cd: ChangeDetectorRef,
		private dataService: DataService
	) {
		this.user = JSON.parse(localStorage.getItem('currentUser'));
		this.getTweets(10);
		this.getFollowing();
	}

	ngOnInit() {}

	public getTweets(limit: number) {
		this.postService.getAllMessages().subscribe(
			complete => {
				this.tweets = complete.json().items;
				console.log(complete.json());
			},
			err => console.log(err)
		);
		this.chrono.reset();
		this.chrono.start();
	}

	public getFollowing() {
		if (this.user) {
			this.userService.getfollowing(this.user.username).subscribe(
				complete => {
					this.following = complete.json().items;
					this.dataService.changeFollowing(this.following);
					localStorage.setItem('following', JSON.stringify(this.following));
					//console.log(complete.json());
				},
				err => console.log(err)
			);
		}
	}
}
