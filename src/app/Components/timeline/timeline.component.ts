import { Component, OnInit } from '@angular/core';
import { Post } from 'app/Models/post';
import { Timer } from '../../Models/timer';
import { UserService } from '../../Services/user.service/user.service';
import { PostService } from 'app/Services/post.service/post.service';
import { DataService } from 'app/Services/data.service/data.service';

@Component({
	selector: 'app-timeline',
	templateUrl: './timeline.component.html',
	styleUrls: ['./timeline.component.css']
})
export class TimelineComponent implements OnInit {
	list: Array<Post>;
	limit: number;
	chrono: number;

	constructor(private userService: UserService, private postService: PostService, private dataService: DataService) {
		this.chrono = 0;
		this.getTimeLine(10);
	}

	ngOnInit() {}

	public getTimeLine(limit: number) {
		let dateDeb = new Date();
		this.userService.getTimeline(JSON.parse(localStorage.getItem('currentUser')).username, limit).subscribe(
			complete => {
				let dateFin = new Date();
				this.chrono = dateFin.getTime() - dateDeb.getTime();
				this.list = complete.json().items;
			},
			err => {
				console.log(err);
			}
		);
	}
}
