import { Component, OnInit } from '@angular/core';
import { UserService } from '../../Services/user.service/user.service';
import { User } from 'app/Models/user';

@Component({
	selector: 'app-scrollfollower',
	templateUrl: './scrollfollower.component.html',
	styleUrls: ['./scrollfollower.component.css']
})
export class ScrollfollowerComponent implements OnInit {
	follows: Array<User>;

	constructor(private userservice: UserService) {
		this.getFollowers(JSON.parse(localStorage.getItem('currentUser')).username);
	}

	ngOnInit() {}

	getFollowers(username: String) {
		this.userservice.getfollowing(username).subscribe(
			complete => {
				console.log(complete.json().items);
				this.follows = complete.json().items;
			},
			err => {
				console.log(err);
			}
		);
	}
}
