import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ScrollfollowerComponent } from './scrollfollower.component';

describe('ScrollfollowerComponent', () => {
  let component: ScrollfollowerComponent;
  let fixture: ComponentFixture<ScrollfollowerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ScrollfollowerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ScrollfollowerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
