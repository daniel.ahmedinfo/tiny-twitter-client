import { Component, OnInit } from '@angular/core';
import { Input } from '@angular/core/src/metadata/directives';
import { EventEmitter } from 'events';
import { Router } from '@angular/router';
import { DataService } from 'app/Services/data.service/data.service';
import { User } from 'app/Models/user';
import { TweetComponent } from 'app/Components/tweet/tweet.component';
import { DialogService } from 'ng2-bootstrap-modal';

@Component({
	selector: 'app-header',
	templateUrl: './header.component.html',
	styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
	user: User;

	closeResult: string;

	constructor(private router: Router, private dataService: DataService, private dialogService: DialogService) {}

	ngOnInit() {
		this.user = JSON.parse(localStorage.getItem('currentUser'));
	}

	/*toTweet() {
		this.router.navigateByUrl('tweet');
	}*/

	toTimeLine() {
		this.router.navigateByUrl('timeline');
	}

	toHome() {
		this.router.navigateByUrl('');
	}

	open() {
		let disposable = this.dialogService
			.addDialog(TweetComponent, {
				title: 'Tweet',
				message: 'Confirm message'
			})
			.subscribe(isConfirmed => {});
		//We can close dialog calling disposable.unsubscribe();
		//If dialog was not closed manually close it by timeout
		/*setTimeout(() => {
			disposable.unsubscribe();
		}, 3000000);*/
	}
}
