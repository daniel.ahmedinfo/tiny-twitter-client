import { Component, OnInit } from '@angular/core';
import { User } from 'app/Models/user';
import { UserService } from './../../Services/user.service/user.service';
import { Timer } from 'app/Models/timer';
import { ActivatedRoute } from '@angular/router';
import { DataService } from '../../Services/data.service/data.service';

@Component({
	selector: 'app-profile',
	templateUrl: './profile.component.html',
	styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {
	chrono: Timer;

	user: User;
	

	welcomeText: String = 'Welcome !'; // Welcome par defaut
	//Biography , Location , Date de naissance

	constructor(private userService: UserService, private dataService: DataService, route: ActivatedRoute) {}

	ngOnInit() {
		this.user = JSON.parse(localStorage.getItem('currentUser'));
	}
}
