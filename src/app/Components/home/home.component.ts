import { Component, OnInit } from '@angular/core';
import { UserService } from '../../Services/user.service/user.service';
import { User } from 'app/Models/user';

@Component({
	selector: 'app-home',
	templateUrl: './home.component.html',
	styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
	constructor(private userService: UserService) {}
	users: Array<User>;

	ngOnInit() {
		//this.getUsers(true);
	}

	getUsers(top: boolean) {
		this.userService.getAllUsers(top).subscribe(
			complete => {
				this.users = complete.json().items;
				console.log(complete.json());
			},
			err => {
				console.log(err);
			}
		);
	}
}
