import { Component, OnInit } from '@angular/core';
import { UserService } from '../../Services/user.service/user.service';
import { DataService } from 'app/Services/data.service/data.service';
import { Router } from '@angular/router';

@Component({
	selector: 'app-login',
	templateUrl: './login.component.html',
	styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
	username: String;
	password: String;

	constructor(private userService: UserService, private dataService: DataService, private router: Router) {}

	ngOnInit() {
		localStorage.clear();
	}

	getProfile() {
		this.userService.getProfile(this.username).subscribe(
			complete => {
				this.dataService.changeUser(complete.json());
				localStorage.setItem('currentUser', JSON.stringify(complete.json()));
				console.log(complete.json());
				this.router.navigate(['timeline']);
			},
			err => {
				console.log(err);
			}
		);
	}
}
