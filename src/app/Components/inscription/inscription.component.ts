import { Component, OnInit } from '@angular/core';
import { UserService } from '../../Services/user.service/user.service';
import { User } from 'app/Models/user';
import { Timer } from '../../Models/timer';
import { Router } from '@angular/router';

@Component({
	selector: 'app-inscription',
	templateUrl: './inscription.component.html',
	styleUrls: ['./inscription.component.css']
})
export class InscriptionComponent implements OnInit {
	chrono: Timer;
	user: User;

	constructor(private userService: UserService, private router: Router) {
		this.chrono = new Timer();
		this.user = new User();
	}

	ngOnInit() {}
	public addUser() {
		this.chrono.reset();
		this.chrono.start();
		this.userService.addUser(this.user).subscribe(
			complete => {
				this.chrono.stop();
				console.log(complete.json());
				this.router.navigateByUrl('login');
			},
			err => {
				console.log(err);
			}
		);
	}
}
