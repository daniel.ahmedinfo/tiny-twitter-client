import { Component } from "@angular/core";
import { UserService } from "./Services/user.service/user.service";
import { OnInit } from "@angular/core/src/metadata/lifecycle_hooks";
import { User } from "app/Models/user";
import { PostService } from "./Services/post.service/post.service";
import { Timer } from "app/Models/timer";
import { AuthService } from "./Services/auth.service";
@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.css"]
})
export class AppComponent implements OnInit {
  constructor(
    private us: UserService,
    private ps: PostService,
    public auth: AuthService
  ) {
    auth.handleAuthentication();
  }
  title = "app works!";
  chrono: Timer = new Timer();

  ngOnInit() {}
}
