import { RouterModule, Routes } from '@angular/router';
import { TweetsComponent } from 'app/Components/tweets/tweets.component';
import { TweetComponent } from 'app/Components/tweet/tweet.component';
import { HomeComponent } from './Components/home/home.component';
import { TimelineComponent } from './Components/timeline/timeline.component';
import { ProfileComponent } from 'app/Components/profile/profile.component';
import { LoginComponent } from './Components/login/login.component';
import { AuthGuardService } from 'app/Services/auth.guard.service/auth.guard.service';
import { CallbackComponent } from 'app/Components/callback/callback.component';
import { InscriptionComponent } from 'app/Components/inscription/inscription.component';

const appRoutes: Routes = [
	{ path: '', component: HomeComponent, pathMatch: 'full' },
	{
		path: 'timeline',
		component: TimelineComponent,
		canActivate: [AuthGuardService]
	},
	{ path: 'tweet', component: TweetComponent, canActivate: [AuthGuardService] },
	{ path: 'login', component: LoginComponent },
	{ path: 'register', component: InscriptionComponent },
	{ path: 'callback', component: CallbackComponent },

	{ path: '**', redirectTo: 'login' }
];

export const routing = RouterModule.forRoot(appRoutes, { enableTracing: true });
