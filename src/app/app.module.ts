import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { FormsModule } from "@angular/forms";
import { HttpModule } from "@angular/http";

import { AppComponent } from "./app.component";
import { UserService } from "./Services/user.service/user.service";
import { AuthService } from "./Services/auth.service";

import { PostService } from "./Services/post.service/post.service";
import { ProfileComponent } from "app/Components/profile/profile.component";
import { TweetsComponent } from "./Components/tweets/tweets.component";
import { TweetComponent } from "app/Components/tweet/tweet.component";
import { routing } from "./app.routing";
import { FooterComponent } from "./Components/footer/footer.component";
import { HomeComponent } from "app/Components/home/home.component";
import { TimelineComponent } from "app/Components/timeline/timeline.component";
import { HeaderComponent } from "app/Components/header/header.component";
import { FollowComponent } from "./Components/follow/follow.component";
import { InscriptionComponent } from "./Components/inscription/inscription.component";
import { DataService } from "./Services/data.service/data.service";
import { LoginComponent } from "./Components/login/login.component";
import { AuthGuardService } from "app/Services/auth.guard.service/auth.guard.service";
import { ScrollfollowerComponent } from "./Components/scrollfollower/scrollfollower.component";
import { CallbackComponent } from "./Components/callback/callback.component";
import { CommonModule } from "@angular/common";
import { BootstrapModalModule } from "ng2-bootstrap-modal";

@NgModule({
  declarations: [
    AppComponent,
    ProfileComponent,
    TweetsComponent,
    TweetComponent,
    FooterComponent,
    HomeComponent,
    TimelineComponent,
    HeaderComponent,
    FollowComponent,
    InscriptionComponent,
    LoginComponent,
    ScrollfollowerComponent,
    CallbackComponent
  ],
  imports: [
    routing,
    BrowserModule,
    FormsModule,
    HttpModule,
    CommonModule,
    BootstrapModalModule
  ],

  providers: [
    UserService,
    PostService,
    DataService,
    AuthGuardService,
    AuthService
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
