export const environment = {
	production: true,
	clientId: '[CLIENT_ID]',
	apiUrl: 'https://tiny-twitter-189923.appspot.com/_ah/api',
	personEndpoint: '/person/v1/users',
	postEndpoint: '/post/v1'
};
